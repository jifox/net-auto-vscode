# Network Automation - VScode

Container for network automatization projects using Microsoft vscode devcontainer

```bash
FROM jifox01/net-auto-base:latest
```

## Customizing

Use the following build arguments (--build-arg) to customize this container

|Argument|Default|Remarks|
|--------|-------|-------|
| DOCKER_GROUP_ID | 998 | Group id of group "docker" |
| USERNAME | vscode | Username for non-root user |
| USER_UID | 1000 | default user id. For bind mount volumes it shoud match the user id on the docker node `id -u` |
| USER_GID | ${USER_UID} | Default group id matches the userid. For bind mount volumes it shoud match the group-id on the docker node `id -g` |

## Python 3.9.0 LIbraries installed

```bash
adal==1.2.1
aiohttp==3.7.2
ansible==2.10.3
ansible-base==2.10.3
appdirs==1.4.4
argcomplete==1.12.1
arrow==0.17.0
astroid==2.4.2
async-timeout==3.0.1
asyncio==3.4.3
asyncssh==2.4.2
attrs==20.3.0
bcrypt==3.2.0
binaryornot==0.4.4
black==20.8b1
botbuilder-core==4.11.0
botbuilder-schema==4.11.0
botframework-connector==4.11.0
cached-property==1.5.2
certifi==2020.11.8
cffi==1.14.3
chardet==3.0.4
ciscoconfparse==1.5.19
click==7.1.2
colorama==0.4.4
commonmark==0.9.1
cookiecutter==1.7.0
cryptography==3.2.1
distro==1.5.0
dnspython==2.0.0
docker==4.3.1
docker-compose==1.27.4
dockerpty==0.4.1
docopt==0.6.2
furl==2.1.0
future==0.18.2
idna==2.10
ISE==0.1.2
isodate==0.6.0
isort==5.6.4
Jinja2==2.11.2
jinja2-time==0.2.0
jmespath==0.10.0
jsonpickle==1.2
jsonschema==3.2.0
junos-eznc==2.5.4
lazy-object-proxy==1.4.3
lxml==4.6.1
MarkupSafe==1.1.1
mccabe==0.6.1
msal==1.2.0
msrest==0.6.10
multidict==5.0.2
mypy==0.790
mypy-extensions==0.4.3
napalm==3.2.0
napalm-ansible==1.1.0
ncclient==0.6.9
netaddr==0.8.0
netmiko==3.3.2
nornir==3.0.0
nornir-ansible==2020.9.26
nornir-jinja2==0.1.0
nornir-napalm==0.1.1
nornir-netbox==0.1.1
nornir-netmiko==0.1.1
nornir-scrapli==2020.11.1
nornir-utils==0.1.1
ntc-templates==1.6.0
oauthlib==3.1.0
objectpath==0.6.1
orderedmultidict==1.0.1
packaging==20.4
paramiko==2.7.2
passlib==1.7.4
pathspec==0.8.1
poyo==0.5.0
pycparser==2.20
pyeapi==0.8.4
Pygments==2.7.2
PyJWT==1.5.3
pylint==2.6.0
PyNaCl==1.4.0
pynetbox==5.1.0
pyparsing==2.4.7
pyrsistent==0.17.3
pyserial==3.4
python-dateutil==2.8.1
python-dotenv==0.15.0
PyYAML==5.3.1
regex==2020.11.13
requests==2.25.0
requests-oauthlib==1.3.0
rich==9.2.0
rope==0.18.0
ruamel.yaml==0.16.12
scp==0.13.3
scrapli==2020.10.10
scrapli-asyncssh==2020.10.10
scrapli-community==2020.9.19
scrapli-netconf==2020.10.24
six==1.15.0
tenacity==6.2.0
textfsm==1.1.0
texttable==1.6.3
toml==0.10.2
transitions==0.8.5
typed-ast==1.4.1
typing-extensions==3.7.4.3
urllib3==1.26.2
websocket-client==0.57.0
whichcraft==0.6.1
wrapt==1.12.1
xmltodict==0.12.0
yamllint==1.25.0
yamlordereddictloader==0.4.0
yapf==0.30.0
yarl==1.6.3
yq==2.11.1
```