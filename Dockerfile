FROM jifox01/net-auto-base:latest

ARG DOCKER_GROUP_ID=998
ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=${USER_UID}

ENV DOCKER_GROUP_ID=${DOCKER_GROUP_ID}
ENV USERNAME=${USERNAME}
ENV USER_UID=${USER_UID}
ENV USER_GID=${USER_GID}

# Add group docker and non-root user
COPY useradd.sh /tmp/useradd.sh
COPY ssh_conf /tmp/ssh_conf
RUN /tmp/useradd.sh && rm /tmp/useradd.sh

# Create default working directory 
RUN mkdir -p /workspaces \
    && chown -R ${USER_UID}:${USER_GID} /workspaces

# [Option] Install Node.js
ARG INSTALL_NODE="true"
ARG NODE_VERSION="lts/*"
ENV NVM_DIR=/usr/local/share/nvm
ENV NVM_SYMLINK_CURRENT=true \
    PATH=${NVM_DIR}/current/bin:${PATH}
RUN if [ "$INSTALL_NODE" = "true" ]; then \
       bash /tmp/library-scripts/common-debian.sh "${INSTALL_ZSH}" "${USERNAME}" "${USER_UID}" "${USER_GID}" "${UPGRADE_PACKAGES}" \
       bash /tmp/library-scripts/node-debian.sh "${NVM_DIR}" "${NODE_VERSION}" "${USERNAME}" ;\
    fi \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/library-scripts

USER ${USERNAME}
WORKDIR /workspaces

CMD [ "sleep", "infinity" ]
