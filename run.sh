#!/bin/bash

# Copy this script to /usr/local/bin and create
# pushd /usr/local/bin
# use ln /usr/bin/ansible.sh /usr/bin/ansible-playook

COMMAND=`basename "$0"`

if [ "${COMMAND}" == "run.sh" ] ; then
    # use arguments only
    docker run \
        --rm \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        --volume ${PWD}:/app \
        --volume /home/${USER}:/home/${USER} \
        --volume ${PWD}/playbooks:/home/${USERNAME}/playbooks \
        --name ansible_${USER} \
        -it ansible_${USER} $@
else
    # use the static link name as command.
    docker run \
        --rm \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        --volume ${PWD}:/app \
        --volume /home/${USER}:/home/${USER} \
        --volume ${PWD}/playbooks:/home/${USERNAME}/playbooks \
        --name ansible_${USER} \
        -it ansible_${USER} ${COMMAND} $@
fi