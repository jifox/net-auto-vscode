#!/bin/bash

set -e

# environment variables used
# DOCKER_GROUP_ID, USER_GID, USER_UID, USERNAME

if ! grep -q docker /etc/group ; then
    groupadd --gid ${DOCKER_GROUP_ID} docker
fi

# If non-root user
if [ ${USER_GID} != 0 ] ; then
    if ! grep -q ${USERNAME} /etc/group ; then
        groupadd --gid ${USER_GID} ${USERNAME}
    fi
    if ! grep -q ${USERNAME} /etc/passwd ; then
        useradd \
            --home-dir /home/${USERNAME} \
            --uid ${USER_UID} \
            --gid ${USER_GID} \
            --groups docker \
            --create-home \
            ${USERNAME}
        chown -R ${USER_UID}:${USER_GID} /home/${USERNAME}
        # Allow passwordless sudo 
        echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME
        chmod 0440 /etc/sudoers.d/$USERNAME
        # Set random password for user to enable account
        echo vscode:$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) | chpasswd
    fi
    HOMEDIR=/home/${USERNAME}
else
    HOMEDIR=/root
fi

mkdir -p ${HOMEDIR}/.ssh
cp /tmp/ssh_conf ${HOMEDIR}/.ssh/config
chmod -R 700 ${HOMEDIR}/.ssh
chown -R ${USER_UID}:${USER_GID} ${HOMEDIR}
mkdir -p /install/playbooks \
mkdir -p /workspaces \
